<?php

function views_ajax_autocomplete_taxonomy_family_restrict($vid, $parents, $children, $siblings, $bundle, $tags_typed = '') {
  // The user enters a comma-separated list of tags. We only autocomplete the last tag.
  $tags_typed = drupal_explode_tags($tags_typed);
  $tag_last = drupal_strtolower(array_pop($tags_typed));

  $matches = array();
  $tags_return = array();
  if ($tag_last != '') {
    // First we get any term matching the input, limited to the top 30 items.
    $query = db_select('taxonomy_term_data', 't');
    $query->addTag('translatable');
    $query->addTag('term_access');

    // Do not select already entered terms.
    if (!empty($tags_typed)) {
      $query->condition('t.name', $tags_typed, 'NOT IN');
    }
    // Select rows that match by term name.
    $valid_tags_return = $query
      ->fields('t', array('tid', 'name'))
      ->condition('t.vid', $vid)
      ->condition('t.name', '%' . db_like($tag_last) . '%', 'LIKE')
      ->range(0, 30)
      ->execute()
      ->fetchAllKeyed();

    // Next we get related terms and add them to the array that we will do a query on next.
    $valid_terms = $valid_tags_return;
    $option_defs = array(
      'options_add_parents' => (!empty($parents)) ? 1 : 0,
      'options_add_children' => (!empty($children)) ? 1 : 0,
      'options_add_siblings' => (!empty($siblings)) ? 1 : 0,
    );
    $valid_tags_return = _views_term_family_get_family_options($valid_terms, $option_defs);
    asort($valid_tags_return);
    // Reduce the array to just the name values keyed by a numeric index.
    $valid_tags = array_values($valid_tags_return);

    if(!empty($valid_tags)) {
      // Now we have validated term names and their relations so we check if any of these are on content.
      $query = db_select('taxonomy_index', 'tn');
      $query->leftJoin('taxonomy_term_data', 't', 'tn.tid = t.tid');
      $query->leftJoin('taxonomy_vocabulary', 'tv', 't.vid = tv.vid');
      $query->innerJoin('node', 'nd', 'tn.nid = nd.nid');

      //$query = db_select('taxonomy_term_data', 't');
      $query->addTag('translatable');
      $query->addTag('term_access');

      // Do not select already entered terms.
      if (!empty($tags_typed)) {
        $query->condition('t.name', $tags_typed, 'NOT IN');
      }
      if (!empty($bundle)) {
        $query->condition('nd.type', $bundle);
      }
      // Select rows that match by term name.
      $tags_return = $query
        ->fields('t', array('tid', 'name'))
        ->condition('t.vid', $vid)
        ->condition('t.name', $valid_tags, 'IN')
        ->range(0, 10)
        ->execute()
        ->fetchAllKeyed();

      $prefix = count($tags_typed) ? drupal_implode_tags($tags_typed) . ', ' : '';

      // Get related terms and add them to the limited options
      $terms = $tags_return;
      $option_defs = array(
        'options_add_parents' => (!empty($parents)) ? 1 : 0,
        'options_add_children' => (!empty($children)) ? 1 : 0,
        'options_add_siblings' => (!empty($siblings)) ? 1 : 0,
      );
      $tags_return = _views_term_family_get_family_options($terms, $option_defs);
      asort($tags_return);
    }

    $term_matches = array();
    foreach ($tags_return as $tid => $name) {
      $n = $name;
      // Term names containing commas or quotes must be wrapped in quotes.
      if (strpos($name, ',') !== FALSE || strpos($name, '"') !== FALSE) {
        $n = '"' . str_replace('"', '""', $name) . '"';
      }
      // Add term name to list of matches.
      $term_matches[$prefix . $n] = check_plain($name);
    }
  }

  drupal_json_output($term_matches);
}