<?php

/**
 * @file
 * Definition of views_handler_filter_term_node_tid_family.
 */

/**
 * Filter handler to get a given taxonomy terms immediate family and filter by each term in it.
 *
 */
class views_handler_filter_term_node_tid_family extends views_handler_filter_term_node_tid {
  function option_definition() {
    $options = parent::option_definition();

    $options['parents'] = array('default' => FALSE, 'bool' => TRUE);
    $options['children'] = array('default' => FALSE, 'bool' => TRUE);
    $options['siblings'] = array('default' => FALSE, 'bool' => TRUE);

    return $options;
  }

  function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);

    $form['parents'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add Parents to Query'),
      '#default_value' => $this->options['parents'],
      '#description' => t('The parents will match nodes tagged with terms higher in the hierarchy. For example, if you have the term "fruit" and a child term "apple", with parents selected filtering for the term "apple" will get nodes that are tagged with "fruit" as well as "apple".'),
    );

    $form['children'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add Children to Query'),
      '#default_value' => $this->options['children'],
      '#description' => t('The children will match nodes tagged with terms lower in the hierarchy. For example, if you have the term "apple" and a child term "honey crisp", with children selected filtering for the term "apple" will get nodes that are tagged with "honey crisp" as well as "apple".'),
    );

    $form['siblings'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add Siblings to Query'),
      '#default_value' => $this->options['siblings'],
      '#description' => t('The siblings will match nodes tagged with terms in the same level of a hierarchy. For example, if you have the term "apple" and a sibling term "bannana", with siblings selected filtering for the term "apple" will get nodes that are tagged with "bannana" as well as "apple".'),
    );
  }

  function exposed_validate(&$form, &$form_state) {
    parent::exposed_validate($form, $form_state);
    if (isset($this->validated_exposed_input)) {
      $terms = $this->validated_exposed_input;
      $options = $this->options;
      $family = _views_term_family_get_family($terms, $options);
      $this->validated_exposed_input = (!empty($family)) ? $family : $this->validated_exposed_input;      
    }
  }
}
