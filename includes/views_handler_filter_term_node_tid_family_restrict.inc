<?php

/**
 * @file
 * Definition of views_handler_filter_term_node_tid_family_restrict.
 */

/**
 * Filter handler to restrict available options to terms on nodes allowing one to add immediate term relations to the options
 * and get a given taxonomy terms immediate family and filter by each term in it.
 *
 */
class views_handler_filter_term_node_tid_family_restrict extends views_handler_filter_term_node_tid_family {
  function option_definition() {
    $options = parent::option_definition();

    $options['onbundle'] = array('default' => FALSE, 'bool' => TRUE);
    $options['bundle'] = array('default' => '');

    $options['options_add_parents'] = array('default' => FALSE, 'bool' => TRUE);
    $options['options_add_children'] = array('default' => FALSE, 'bool' => TRUE);
    $options['options_add_siblings'] = array('default' => FALSE, 'bool' => TRUE);

    return $options;
  }

  function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);

    $entity_info = entity_get_info('node');
    foreach ($entity_info['bundles'] as $bundlekey => $bundleinfo) {
      $bundles[$bundlekey] = $bundlekey;
    }
    $form['options_add_parents'] = array(
      '#type' => 'checkbox',
      '#title' => t('Options Include Parents'),
      '#default_value' => $this->options['options_add_parents'],
      '#description' => t('This will allow the autocomplete/select options to show parents of terms on nodes. This will not stop a term from showing up that is a sibling or child of another term when those options have been allowed'),
    );

    $form['options_add_children'] = array(
      '#type' => 'checkbox',
      '#title' => t('Options Include Children'),
      '#default_value' => $this->options['options_add_children'],
      '#description' => t('This will allow the autocomplete/select options to not show children of terms on nodes. This will not stop a term from showing up that is a parent or sibling of another term when those options have been allowed'),
    );

    $form['options_add_siblings'] = array(
      '#type' => 'checkbox',
      '#title' => t('Options Include Siblings'),
      '#default_value' => $this->options['options_add_siblings'],
      '#description' => t('This will allow the autocomplete/select options to not show siblings of terms on nodes. This will not stop a term from showing up that is a parent or child of another term, when thpse options have been allowed'),
    );

    $form['onbundle'] = array(
      '#type' => 'checkbox',
      '#title' => t('Limit Available Options to Terms on Specific Content Types'),
      '#default_value' => $this->options['onbundle'],
      '#description' => t('Limit available term options to terms appearing on specific Content Types'),
      '#weight' => -10,
    );
    $form['bundle'] = array(
      '#type' => 'radios',
      '#title' => t('Limit Available Options to Terms on Specific Content Types'),
      '#options' => $bundles,
      '#default_value' => $this->options['bundle'],
      '#description' => t('The terms returned for available options will be limited to terms on the bundle specified here, otherwise the selected vocabulary terms on any node will be available to select.'),
      '#dependency' => array(
        'edit-options-onbundle' => array(1)
      ),
      '#weight' => -9,
    );
  }

  function value_form(&$form, &$form_state) {
    $bundle = empty($this->options['bundle']) ? 0 : $this->options['bundle'];
    $vocabulary = taxonomy_vocabulary_machine_name_load($this->options['vocabulary']);
    if (empty($vocabulary) && $this->options['limit']) {
      $form['markup'] = array(
        '#markup' => '<div class="form-item">' . t('An invalid vocabulary is selected. Please change it in the options.') . '</div>',
      );
      return;
    }

    if ($this->options['type'] == 'textfield') {
      $default = '';
      if ($this->value) {
        $result = db_select('taxonomy_term_data', 'td')
          ->fields('td')
          ->condition('td.tid', $this->value)
          ->execute();
        foreach ($result as $term) {
          if ($default) {
            $default .= ', ';
          }
          $default .= $term->name;
        }
      }

      $form['value'] = array(
        '#title' => $this->options['limit'] ? t('Select terms from vocabulary @voc', array('@voc' => $vocabulary->name)) : t('Select terms'),
        '#type' => 'textfield',
        '#default_value' => $default,
      );

      // Setup our options selection criteria
      $options_add_parents = empty($this->options['options_add_parents']) ? 0 : 1;
      $options_add_children = empty($this->options['options_add_children']) ? 0 : 1;
      $options_add_siblings = empty($this->options['options_add_siblings']) ? 0 : 1;
      
      if ($this->options['limit']) {
        $form['value']['#autocomplete_path'] = 'admin/views/ajax/autocomplete/taxonomy_family_restrict/' . $vocabulary->vid . '/' . $options_add_parents . '/' . $options_add_children . '/' . $options_add_siblings . '/' . $bundle;
      }
      else {
        $form['value']['#autocomplete_path'] = 'admin/views/ajax/autocomplete/taxonomy_family_restrict/0/' . $options_add_parents . '/' . $options_add_children . '/' . $options_add_siblings . '/' . $bundle;
      }
    }
    else {
      if (!empty($this->options['hierarchy']) && $this->options['limit']) {
        $tree = taxonomy_get_tree($vocabulary->vid);
        $options = array();

        if ($tree) {
          foreach ($tree as $term) {
            $choice = new stdClass();
            $choice->option = array($term->tid => str_repeat('-', $term->depth) . $term->name);
            $options[] = $choice;
          }
        }
      }
      else {
        $options = array();
        $query = db_select('taxonomy_index', 'tn');
        $query->leftJoin('taxonomy_term_data', 'td', 'tn.tid = td.tid');
        $query->leftJoin('taxonomy_vocabulary', 'tv', 'td.vid = tv.vid');
        $query->innerJoin('node', 'nd', 'tn.nid = nd.nid');
        $query->fields('td');
        $query->orderby('tv.weight');
        $query->orderby('tv.name');
        $query->orderby('td.weight');
        $query->orderby('td.name');
        $query->addTag('term_access');
        if ($this->options['limit']) {
          $query->condition('tv.machine_name', $vocabulary->machine_name);
        }
        if ($this->options['onbundle'] && !empty($this->options['bundle'])) {
          $query->condition('nd.type', $this->options['bundle']);
        }
        $result = $query->execute();
        foreach ($result as $term) {
          $options[$term->tid] = $term->name;
        }
      }

      // Get related terms and add them to the limited options
      $terms = $options;
      $option_defs = $this->options;
      $options = _views_term_family_get_family_options($terms, $option_defs);
      asort($options);

      $default_value = (array) $this->value;

      if (!empty($form_state['exposed'])) {
        $identifier = $this->options['expose']['identifier'];

        if (!empty($this->options['expose']['reduce'])) {
          $options = $this->reduce_value_options($options);

          if (!empty($this->options['expose']['multiple']) && empty($this->options['expose']['required'])) {
            $default_value = array();
          }
        }

        if (empty($this->options['expose']['multiple'])) {
          if (empty($this->options['expose']['required']) && (empty($default_value) || !empty($this->options['expose']['reduce']))) {
            $default_value = 'All';
          }
          elseif (empty($default_value)) {
            $keys = array_keys($options);
            $default_value = array_shift($keys);
          }
          // Due to #1464174 there is a chance that array('') was saved in the admin ui.
          // Let's choose a safe default value.
          elseif ($default_value == array('')) {
            $default_value = 'All';
          }
          else {
            $copy = $default_value;
            $default_value = array_shift($copy);
          }
        }
      }
      $form['value'] = array(
        '#type' => 'select',
        '#title' => $this->options['limit'] ? t('Select terms from vocabulary @voc', array('@voc' => $vocabulary->name)) : t('Select terms'),
        '#multiple' => TRUE,
        '#options' => $options,
        '#size' => min(9, count($options)),
        '#default_value' => $default_value,
      );

      if (!empty($form_state['exposed']) && isset($identifier) && !isset($form_state['input'][$identifier])) {
        $form_state['input'][$identifier] = $default_value;
      }
    }


    if (empty($form_state['exposed'])) {
      // Retain the helper option
      $this->helper->options_form($form, $form_state);
    }
  }
}
